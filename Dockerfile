# Step 1: Builder image
FROM node:14.15.1-alpine3.12 AS builder
COPY [".eslintrc.js", ".eslintignore", "tsconfig.json", "gulpfile.js", "package.json", "package-lock.json", "/command/"]
COPY src /command/src
COPY spec /command/spec
WORKDIR /command
RUN npm install
RUN npm test
RUN npm run release

# After the build, this only installs the libraries used in production,
# not the ones installed with the `--save-dev` parameter
RUN rm -rf node_modules
RUN npm install --production

# Install the "pkg" package
RUN npm install -g pkg pkg-fetch
ENV NODE node12
ENV PLATFORM alpine
ENV ARCH x64
RUN /usr/local/bin/pkg-fetch ${NODE} ${PLATFORM} ${ARCH}

# Package app without dependencies
RUN /usr/local/bin/pkg --targets ${NODE}-${PLATFORM}-${ARCH} /command/dist/index.js -o greeter.bin



# Step 2: Runtime image
FROM alpine:3.12
RUN apk add --no-cache libstdc++
COPY --from=builder /command/greeter.bin /usr/local/bin/greeter
ENTRYPOINT [ "greeter" ]
