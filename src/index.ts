import { Customer } from './customer'
import { help, Option, program } from 'commander'

let customerName = ''
const characters = ['C3PO', 'R2-D2', 'ackbar', 'armadillo', 'bunny',
  'cat', 'default', 'doge', 'mona-lisa', 'snoopy']

const optionA = new Option('-c, --character <character>', 'greeter')
  .choices(characters)

const optionB = new Option('-a, --action <action>', 'thing to do')
  .choices(['talk', 'think'])
  .default('talk')

program
  .name('Cowsay Greeter')
  .version('1.0.0')
  .arguments('[name]')
  .action(name => {
    if (!name) help()
    customerName = name
  })
  .addOption(optionA)
  .addOption(optionB)

program.parse(process.argv)
const options = program.opts()

// Use the library and log output
const cust = new Customer(customerName)
const greeting = cust.greet(options.character, options.action)
console.log(greeting)
