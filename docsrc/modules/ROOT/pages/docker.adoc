= Docker

This project can be used with Docker and Podman:

image::docker.svg[width=150]

image::podman.svg[width=150]

[source]
--
$ docker run --rm quay.io/akosma/greeter:1.0 "John Smith" --character cat --action think
 __________________
( Hello John Smith )
 ------------------
  o
   o                       _
                          / )
                         / /
      //|                \ \
   .-`^ \   .-`````-.     \ \
 o` {|}  \_/         \    / /
 '--,  _ //   .---.   \  / /
   ^^^` )/  ,/     \   \/ /
        (  /)      /\/   /
        / / (     / (   /
    ___/ /) (  __/ __\ (
   (((__)((__)((__(((___)

--
